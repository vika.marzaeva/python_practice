print('My first practice day')
#лексикографическое сравнение
#чем дальше символ от начала алфавита, тем он больше (код в таблице символов больше)
print('abc'>'acb')
#все символы равнозначны
print(len('assdf')<len('sdf'))

#условные консрукции
a=int(input())
if a%2==0:
    print(a, '- число четное')
else:
    print(a, '- число нечетное')
#как только находится истинное условие - все дальнейшие проверки не происходят.
#любое ненулевое число всегда вернет True
#любая ненулевая строка всегда вернет True
#нулевое число, пустая строка, пустные списки, словари, множества - они все вернут False
salary=300000
print('Ваша зарплата составляет '+str(salary)+' условных единий')
print(1+True) #максимальное сохранение информации

my_string='My name is Vika'

print(my_string[1])
print(my_string[-1])
print(my_string[1:9:2])
print(my_string[0:9:2])
print(my_string[9])
print(my_string[1])
print(my_string[::-1])

#форматирование
salary=300000
print(f'Ваша зарплата {salary}, a годовая {salary*12}')
print('bye')
